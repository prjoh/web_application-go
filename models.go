package main

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)


type Role struct {
	ID   primitive.ObjectID `bson:"_id,omitempty"`
	Name string             `bson:"name"`
}

type User struct {
	ID     	 primitive.ObjectID `bson:"_id,omitempty"`
	Name   	 string             `bson:"name"`
	Password string 		  	`bson:"password"`
	Role 	 Role				`bson:"role"`
	BaseID	 string				`bson:"base,omitempty"`
}

type Token struct {
	ID       primitive.ObjectID `bson:"_id,omitempty"`
	UUID     string             `bson:"uuid"`
	UserID   string				`bson:"user_id"`
	ExpireAt time.Time			`bson:"expire_at"`
}
