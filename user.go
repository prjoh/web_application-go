package main

import "net/http"


func (app *App) SomeApi(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("Some User API!\n"))
}

func (app *App) GetExperiments(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("Implement Experiment Read!\n"))
}


func (app *App) DeleteExperiments(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("Implement Experiment Deletion!\n"))
}

func (app *App) CreateExperiment(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("Implement Experiment Creation!\n"))
}


func (app *App) UpdateExperiment(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("Implement Experiment Update!\n"))
}
