#!/bin/bash

openssl genrsa -out app.rsa 4096
openssl rsa -in app.rsa -pubout > app.rsa.pub

mkdir -p ~/.ssh/rsa
mv app.rsa ~/.ssh/rsa
mv app.rsa.pub ~/.ssh/rsa
