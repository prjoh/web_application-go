package main

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"golang.org/x/crypto/bcrypt"
)

// TODO: Return error
func (app *App) InitDB() {
	// Setup DB
	uri := "mongodb://" +
		app.Cnfg.DB.Host + ":" +
		app.Cnfg.DB.Port
	credential := options.Credential{
		Username: app.Cnfg.DB.User,
		Password: app.Cnfg.DB.Password,
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri).SetAuth(credential))
	if err != nil {
		log.Fatal(err)
		return
	}
	app.Client = client
	// Ping the primary
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		log.Fatal(err)
		return
	}
	db := client.Database(app.Cnfg.DB.Name)
	// Keep pointer to database instance
	app.DB = db
	// Init Roles
	roles := db.Collection("roles")
	count, err := roles.CountDocuments(ctx, bson.D{}); if err != nil {
		log.Fatal(err)
		return
	}
	if count == 0 {
		adminRole := Role{ Name: "Admin" }
		userRole := Role{ Name: "User" }
		_, err = roles.InsertMany(ctx, []interface{}{adminRole, userRole}); if err != nil {
			log.Fatal(err)
			return
		}
		app.Logf("app.InitDB   #Initialized roles: Admin, User")
	}
	// Init Admin
	users := db.Collection("users")
	var findAdmin bson.M
	filter := bson.D{{"name", app.Cnfg.Auth.AdminUser}}
	if err := users.FindOne(ctx, filter).Decode(&findAdmin); err != nil {
		if err == mongo.ErrNoDocuments {
			password, err := bcrypt.GenerateFromPassword([]byte(app.Cnfg.Auth.AdminPassword), bcrypt.DefaultCost)
			if err != nil {
				log.Fatalf("bcrypt.GenerateFromPassword err   #%v ", err)
			}
			var adminRole Role
			if err := roles.FindOne(ctx, bson.D{{"name", "Admin"}}).Decode(&adminRole); err != nil {
				log.Fatal(err)
				return
			}
			adminUser := User{
				Name: app.Cnfg.Auth.AdminUser,
				Password: string(password),
				Role: adminRole,
			}
			_, err = users.InsertOne(ctx, adminUser); if err != nil {
				log.Fatal(err)
				return
			}
			app.Logf("app.InitDB   #Initialized admin user.")
		} else {
			log.Fatal(err)
			return
		}
	}
	// Init tokens collection TTL
	tokens := app.DB.Collection("refresh_tokens")
	var ttl int32 = 0
	keys := bson.D{{Key: "expire_at", Value: 1}}
	idx := mongo.IndexModel{Keys: keys, Options: &options.IndexOptions{ExpireAfterSeconds: &ttl}}
	_, err = tokens.Indexes().CreateOne(context.Background(), idx)
	if err != nil {
		log.Fatal(err)
		return
	}
	app.Logf("app.InitDB   #Init of database complete.")
}