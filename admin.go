package main

import (
	"encoding/json"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
)


type CreateUserRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

//type DeleteUserRequest struct {
//	UserID string `json:"userid"`
//}

func (app *App) CreateUser(w http.ResponseWriter, req *http.Request) {
	var newUser CreateUserRequest
	err := json.NewDecoder(req.Body).Decode(&newUser)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(trace() + err.Error()))
		return
	}
	if len(newUser.Username) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(trace() + "No username found in request body."))
		return
	}
	if len(newUser.Password) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(trace() + "No password found in request body."))
		return
	}

	// Check if user name already exists
	users := app.DB.Collection("users")
	filter := bson.D{{"name", newUser.Username}}
	var result bson.M
	if err := users.FindOne(req.Context(), filter).Decode(&result); err != mongo.ErrNoDocuments {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(trace() + "Failed! Username is already in use!"))
		return
	}

	// Get user role
	roles := app.DB.Collection("roles")
	filter = bson.D{{"name", "User"}}
	var userRole Role
	if err := roles.FindOne(req.Context(), filter).Decode(&userRole); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(trace() + err.Error()))
		return
	}

	// Encrypt password
	password, err := bcrypt.GenerateFromPassword([]byte(newUser.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Printf("bcrypt.GenerateFromPassword err   #%v ", err)
	}

	// Create new user
	_, err = users.InsertOne(req.Context(), User{
		Name: newUser.Username,
		Password: string(password),
		Role: userRole,
	}); if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(trace() + err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Created new user!"))
}

//func (app *App) DeleteUser(w http.ResponseWriter, req *http.Request) {
//	// TODO
//}
