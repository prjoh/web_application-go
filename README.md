# Go Web Application

Boilerplate for web application written in Go, with following features:

* MongoDB support 
* JWT Athentication

## Local Development

1. Install the latest version of [Docker](https://www.docker.com/products/docker-desktop).
2. Edit `/etc/hosts` and add the following 2 lines at the bottom:
    1. `127.0.0.1 app.local`
    2. `127.0.0.1 db.local`
3. From the repository call:
    1. `bash ./generate_keys.sh`
    2. `docker-compose -f local-compose.yaml up --build -d`.

You should now be able to visit a locally hosted API at [http://app.local:8080](http://app.local:8080) and a DB management service at [http://db.local:8080](http://db.local:8080).

To stop and remove the running containers, execute:
1. `docker stop $(docker ps -a -q)`
2. `docker rm $(docker ps -a -q)`

## Deployment

1. Clone this repository to your server.
2. From the repository folder, execute the following 2 lines:
    1. `bash ./generate_keys.sh`
    2. `docker-compose up -d`
3. Type `docker logs go-app` and check if the database connection was successful.*

You should now be able to visit the API at [https://app.EXAMPLE.xyz](https://app.EXAMPLE.xyz) and a DB management service at [https://db.EXAMPLE.xyz](https://db.EXAMPLE.xyz).

(* In case of database connection error, type `docker restart haive-api`)
