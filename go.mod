module prjoh/go-app

go 1.16

require (
	github.com/golang-jwt/jwt v3.2.1+incompatible
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	go.mongodb.org/mongo-driver v1.7.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	gopkg.in/yaml.v2 v2.4.0
)
