package main

import (
	"github.com/gorilla/mux"
	"net/http"
)


// Chain applies middlewares to a http.HandlerFunc
func Chain(f http.HandlerFunc, middlewares ...func(http.HandlerFunc) http.HandlerFunc) http.HandlerFunc {
	for _, m := range middlewares {
		f = m(f)
	}
	return f
}

func (app *App) SetupRoutes() http.Handler {
	r := mux.NewRouter()
	// Frontend
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello Go!\n"))
	})
	//r.PathPrefix("/").Handler(http.FileServer(http.Dir("client/"))) // TODO: React frontend

	// Authentication
	r.HandleFunc("/api/auth/signin", app.SignIn).Methods(http.MethodPost)
	r.HandleFunc("/api/auth/refresh_token", app.RefreshToken).Methods(http.MethodPost)

	r.HandleFunc("/api/public", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Public Content!\n"))
	})

	// User API
	userSubrouter := r.PathPrefix("/api/user").Subrouter()
	userSubrouter.HandleFunc("/some_api", Chain(app.SomeApi, app.IsUser, VerifyToken))

	// Admin API
	adminSubrouter := r.PathPrefix("/api/admin").Subrouter()
	adminSubrouter.HandleFunc("/create_user", Chain(app.CreateUser, app.IsAdmin, VerifyToken))

	// Setup CORS handler
	var corsHandler func(http.Handler) http.Handler
	corsHandler = func (h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			//anyone can make a CORS request (not recommended in production
			w.Header().Set("Access-Control-Allow-Origin", "*")
			//only allow GET, POST, and OPTIONS
			w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
			//Since I was building a REST API that returned JSON, I set the content type to JSON here.
			//w.Header().Set("Content-Type", "application/json")
			//Allow requests to have the following headers
			w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization, cache-control")
			//if it's just an OPTIONS request, nothing other than the headers in the response is needed
			if r.Method == "OPTIONS" {
				return
			}
			h.ServeHTTP(w, r)
		})
	}

	return corsHandler(r)
}
