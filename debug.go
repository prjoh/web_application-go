package main

import (
	"runtime"
	"strconv"
)


func trace() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return "[" + frame.File + ":" + strconv.Itoa(frame.Line) + ">>>" + frame.Function + "]: "
}