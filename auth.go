package main

import (
	"context"
	"crypto/rsa"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
)


var (
	verifyKey  *rsa.PublicKey			// openssl genrsa -out app.rsa keysize
	signKey    *rsa.PrivateKey		// openssl rsa -in app.rsa -pubout > app.rsa.pub
)
const refreshTokenValidTime = time.Second * 60 //time.Hour * 24 // TODO: Debug
const authTokenValidTime = time.Second * 30 //time.Minute * 15 // TODO: Debug


func (app *App) InitAuthService() {
	signBytes, err := ioutil.ReadFile(app.Cnfg.Auth.Sign)
	if err != nil { log.Printf("ioutil.ReadFile err   #%v ", err) }

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil { log.Printf("jwt.ParseRSAPrivateKeyFromPEM err   #%v ", err) }

	verifyBytes, err := ioutil.ReadFile(app.Cnfg.Auth.Verify)
	if err != nil { log.Printf("ioutil.ReadFile err   #%v ", err) }

	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil { log.Printf("jwt.ParseRSAPublicKeyFromPEM err   #%v ", err) }
}

/*
 * Middleware
 */

type UserInfo struct {
	UserID string
	Role	 string
}

type AuthToken struct {
	*jwt.StandardClaims
	UserInfo
}

type RefreshToken struct {
	*jwt.StandardClaims
}

func TokenExpired(err *jwt.ValidationError) bool {
	return err.Errors&(jwt.ValidationErrorExpired) != 0
}

func VerifyToken(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.Header["Token"] == nil {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte(trace() + "No token provided!"))
			return
		}

		// Read the token out of the response body
		tokenString := req.Header.Get("Token")

		// Parse the token
		authToken, err := jwt.ParseWithClaims(tokenString, &AuthToken{}, func(token *jwt.Token) (interface{}, error) {
			// since we only use the one private key to sign the tokens,
			// we also only use its public counter part to verify
			return verifyKey, nil
		})
		if err != nil {
			ve, ok := err.(*jwt.ValidationError); if ok && TokenExpired(ve) {
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte("Token expired!"))
				return
			}
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte(trace() + err.Error()))
			return
		}

		claims := authToken.Claims.(*AuthToken)
		ctx := context.WithValue(req.Context(), "UserInfo", claims.UserInfo)
		next(w, req.WithContext(ctx))
	})
}

func CreateAuthToken(user User) (string, error) {
	// create a signer for rsa 256
	t := jwt.New(jwt.GetSigningMethod("RS256"))
	// set our claims
	t.Claims = &AuthToken {
		&jwt.StandardClaims{
			ExpiresAt: time.Now().Add(authTokenValidTime).Unix(),
		},
		UserInfo{
			user.ID.Hex(),
			user.Role.Name,
		},
	}
	// Creat token string
	authTokenString, err := t.SignedString(signKey); if err != nil {
		return "", err
	}
	return authTokenString, err
}

func CreateRefreshToken(uuid string) (string, error) {
	rt := jwt.New(jwt.GetSigningMethod("RS256"))
	rt.Claims = &RefreshToken {
		&jwt.StandardClaims{
			Id: uuid,
			ExpiresAt: time.Now().Add(refreshTokenValidTime).Unix(),
		},
	}
	// Creat token string
	refreshTokenString, err := rt.SignedString(signKey); if err != nil {
		return "", err
	}
	return refreshTokenString, err
}

func (app *App) CreateToken(user User) (string, string, error) {
	authTokenString, err := CreateAuthToken(user); if err != nil {
		return "", "", err
	}
	uuid := uuid.New().String()
	refreshTokenString, err := CreateRefreshToken(uuid); if err != nil {
		return "", "", err
	}
	// Add refresh token to collection
	tokens := app.DB.Collection("refresh_tokens")
	token := Token{UUID: uuid, UserID: user.ID.Hex(), ExpireAt: time.Now().Add(refreshTokenValidTime)}
	_, err = tokens.InsertOne(context.TODO(), token); if err != nil {
		return "", "", err
	}

	return authTokenString, refreshTokenString, err
}

func (app *App) IsAdmin(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		userInfo := req.Context().Value("UserInfo").(UserInfo)
		// Check if user has admin privileges
		if userInfo.Role != "Admin" {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte(trace() + "Require Admin Role!"))
			return
		}
		next(w, req)
	})
}

func (app *App) IsUser(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		userInfo := req.Context().Value("UserInfo").(UserInfo)
		// Check if user has user privileges
		if userInfo.Role != "User" {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte(trace() + "Require User Role!"))
			return
		}
		next(w, req)
	})
}


/*
 * Endpoints
 */

type UserCredentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type TokenPair struct {
	AuthToken		 string `json:"auth_token"`
	RefreshToken string `json:"refresh_token"`
}

func (app *App) SignIn(w http.ResponseWriter, req *http.Request) {
	var userCredentials UserCredentials
	err := json.NewDecoder(req.Body).Decode(&userCredentials)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(trace() + err.Error()))
		return
	}
	if len(userCredentials.Username) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(trace() + "No username found in request body."))
		return
	}
	if len(userCredentials.Password) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(trace() + "No password found in request body."))
		return
	}

	// Find user in db
	users := app.DB.Collection("users")
	filter := bson.D{{"name", userCredentials.Username}}
	var user User
	if err := users.FindOne(req.Context(), filter).Decode(&user); err != nil {
		if err == mongo.ErrNoDocuments {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(trace() + "User Not found."))
			return
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(trace() + err.Error()))
			return
		}
	}

	// Check password
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(userCredentials.Password)); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(trace() + "Invalid Password!"))
		return
	}

	// Create authentication and refresh token
	authTokenString, refreshTokenString, err := app.CreateToken(user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(trace() + err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(TokenPair{
		authTokenString,
		refreshTokenString,
	})
}

func (app *App) RefreshToken(w http.ResponseWriter, req *http.Request) {
	if req.Header["Token"] == nil {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(trace() + "Refresh Token is required!"))
		return
	}

	// Read the token out of the response body
	refreshTokenString := req.Header.Get("Token")

	// Parse the token
	refreshToken, err := jwt.ParseWithClaims(refreshTokenString, &RefreshToken{}, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return verifyKey, nil
	})
	if err != nil {
		ve, ok := err.(*jwt.ValidationError); if ok && TokenExpired(ve) {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("Refresh token was expired. Please make a new signin request"))
			return
		}
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(trace() + err.Error()))
		return
	}

	claims := refreshToken.Claims.(*RefreshToken)
	// Lookup token in db
	tokens := app.DB.Collection("refresh_tokens")
	filter := bson.D{{"uuid", claims.Id}}
	var token Token
	if err := tokens.FindOne(req.Context(), filter).Decode(&token); err != nil {
		if err == mongo.ErrNoDocuments {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte(trace() + "Refresh token is not in database!"))
			return
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(trace() + err.Error()))
			return
		}
	}

	// Lookup token in db
	users := app.DB.Collection("users")
	objectID, err := primitive.ObjectIDFromHex(token.UserID); if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(trace() + "Invalid object ID!"))
		return
	}
	filter = bson.D{{"_id", objectID}}
	var user User
	if err := users.FindOne(req.Context(), filter).Decode(&user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(trace() + err.Error()))
		return
	}
	authTokenString, err := CreateAuthToken(user); if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(trace() + err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(TokenPair{
		authTokenString,
		refreshTokenString,
	})
}
